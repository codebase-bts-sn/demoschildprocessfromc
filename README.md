
# DemosChildProcessFromC

Exemples illustrant comment lancer l'exécution d'un processus depuis un programme C/C++ et interagir avec celui-ci par l'intermédiaire d'un *pipe*.

Ces exemples s'appuient sur les fonctions :

. ``fork()``

. ``execl()``

. ``pipe()``

. ``dup2()``

Pour en apprendre davantage sur les flux de données entre processus, on pourra se référer à l'excellent article de Roslyn McConnell : (Pipes, Forks, & Dups: Understanding Command Execution and Input/Output Data Flow)[https://www.rozmichelle.com/pipes-forks-dups/#pipelines]

L'article (Pipes)[https://riptutorial.com/posix/topic/8082/pipes] du site *RIP Tutorial* donne en outre quelques exemples prêts à l'emploi correspondant à des cas d'usage courants des *pipes*.

## ``read-output-child-process``

Cet exemple exécute dans un processus fils la commande ``date +\%T`` pour lire l'heure et l'affiche ensuite sur la console.

![alt text](img/read-output-child-process.png  "read-output-child-process screenshot")

