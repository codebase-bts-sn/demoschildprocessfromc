/**
 * Démo d'utilisation sous Linux de la fonction `execl()` pour lire l'heure 
 * retournée par la commande `date +\%T` exécutée dans un processus fils.
 *
 * Compilation : g++ read-output-child-process -o read-output-child-process
 * Exécution   : ./read-output-child-process
 * Affichage   : L'heure retournée par la commande `date +\%T` est : 17:45:29
 */
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;

int main(int argc, char *argv[]) {
    // On crée un pipe dont les descripteurs sont stockés dans le tableau `fds`
    int fds[2];
    pipe(fds);
    if(pipe(fds) == -1) {
        cerr << "erreur pipe()" << endl;
        exit(EXIT_FAILURE);
    }

    // On forke le processus
    pid_t pid = fork();
  
    switch(pid) {
        case -1 : // Erreur fork
            cerr << "erreur fork()" << endl;
            exit(EXIT_FAILURE);
            break;

        case 0 : // On est dans le fils
            // On ferme l'extrémité de lecture du pipe car inutilisée
            if (close(fds[ 0 ]) == -1) {
                cerr << "close(fds[0] fils" << endl;
                exit(EXIT_FAILURE);
            }    

            if( fds[ 1 ] != STDOUT_FILENO) {    
                // On clone le descripteur d'écriture du pipe dans stdout (après avoir fermé ce dernier)
                // (=> la sortie standard sera donc désormais redirigée dans le descripteur contenu dans fds[1])
                dup2(fds[1], STDOUT_FILENO);
                // On ferme les descripteurs contenus dans fds[] car ils ne sont plus utiles
                // (rappel : stdout est une copie de fds[1] après l'appel précédent à dup2())
                close(fds[0]);
                close(fds[1]);
                // On exécute la commande
                if (execl("/usr/bin/date", (char *)"date", (char *)"+\%T", NULL) < 0) {
                    cerr << "Erreur d'exécution de la commande `date`" << endl;
                    exit(EXIT_FAILURE);
                }
            }
            break;

        default : // On est dans le parent
            // On clone le descripteur de lecture du pipe dans stdin (après avoir fermé ce dernier)
            // (=> l'entrée standard proviendra donc désormais du descripteur contenu dans fds[0])
            if( fds[ 0 ] != STDIN_FILENO) {    
                dup2(fds[ 0 ], STDIN_FILENO);    
                // On ferme les descripteurs contenus dans fds[] car ils ne sont plus utiles
                // (rappel : stdin est une copie de fds[0] après l'appel précédent à dup2())
                close(fds[ 0 ]);          
                close(fds[ 1 ]);

                // On lit l'heure retournée par le processus fils
                string currentTime;
                cin >> currentTime;

                // On affiche l'heure
                cout << "L'heure retournée par la commande `date +\\%T` est : " << currentTime << endl;
            }
            break;
    }

    /* On attend la fin d'exécution du fils */
    int status;
    pid_t wpid = wait(&status);
    if (wpid == -1) {
        cerr << "erreur wait()" << endl;
        exit(EXIT_FAILURE);
    } else {
        // On retourne le code de sortie du processus fils
        return WIFEXITED(status) ? WEXITSTATUS(status) : -1;
    }
}